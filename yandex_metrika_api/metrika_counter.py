import requests
import pprint
from urllib.parse import urlencode, urljoin

AUTHORISE_URL = 'https://oauth.yandex.ru/authorize'
APP_ID = '2c67bbc00b444c7e85da3a304371ec3f'
TOKEN = 'AQAAAAACkg5ZAAQvKaI0pD6jJEMvgCJvlzzOB5s'

auth_data = {
    'response_type': 'token',
    'client_id': APP_ID
}

#print('?'.join((AUTHORISE_URL, urlencode(auth_data))))

class YandexMetrica(object):
    _METRIKA_STAT_URL = 'https://api-metrika.yandex.ru/stat/v1/'
    _METRIKA_MANAGEMENT_URL =  'https://api-metrika.yandex.ru/management/v1/'
    token = None
    
    def __init__(self, token):
        self.token = token

    def get_params(self):
        return {
            'oauth_token': self.token,
            'pretty': 1
        }
        
    def metrics_count(self, counter_id, metrics):
        url = urljoin(self._METRIKA_STAT_URL, 'data')
        params = self.get_params()
        params['metrics'] = metrics
        params['id'] = counter_id
        response = requests.get(url, params=params)
        #print(response.json())
        #print(response.json()['data'][0]['metrics'])
        return response.json()['data'][0]['metrics']
    
        
    @property
    def counter_list(self):
        url = urljoin(self._METRIKA_MANAGEMENT_URL, 'counters')
        params = self.get_params()
        response = requests.get(url, params=params)
        counter_list = [c['id'] for c in response.json()['counters']]
        return counter_list
    
metrika = YandexMetrica(TOKEN)
metrika.counter_list
metrics = 'ym:s:visits,ym:s:pageviews,ym:s:users'
for counter in metrika.counter_list:
    metrics_result = metrika.metrics_count(counter_id=counter, metrics=metrics)
    metrics_result = [int(n) for n in metrics_result]
    print('Счетчик {}\nПосещений: {}\nПросмотров: {}\nПользователей: {}\n'.format(counter, metrics_result[0], metrics_result[1], metrics_result[2]))