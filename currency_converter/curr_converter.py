import osa


def convert_curr(data_curr, to_unit):
    result_list = []
    URL1 = 'http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL'
    client1 = osa.client.Client(URL1)
    #print(data_curr)
    for data in data_curr.values():
        #print(data[0])
        response = client1.service.ConvertToNum(amount=data[0], fromCurrency=data[1], toCurrency=to_unit, rounding=True)
        result_list.append(int(response))
    return result_list



def read_file_to_dict(filename):
   curr_dict = dict()
   with open(filename) as file:
        for line in file:
            key, value, curr = line.split(" ")
            key = key.rstrip(':')
            curr = curr.rstrip('\n')
            curr_dict[key] = (int(value), curr)
   return curr_dict

def main():
    to_unit = 'RUB'
    curr_dict = read_file_to_dict('curr.txt')
    #print(curr_dict)
    converted_list = convert_curr(curr_dict, to_unit)
    
    print(converted_list)
    print('Overall amount: {} {}'.format(sum(converted_list), to_unit))
main()

