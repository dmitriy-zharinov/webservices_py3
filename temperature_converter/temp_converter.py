import osa


def convert_temperature(data_list, from_unit, to_unit):
    result_list = []
    URL1 = 'http://www.webservicex.net/ConvertTemperature.asmx?WSDL'
    client1 = osa.client.Client(URL1)
    for data in data_list:
        response = client1.service.ConvertTemp(Temperature=data, FromUnit=from_unit, ToUnit=to_unit)
        result_list.append(round(response, 2))
    return result_list


def read_file_to_temps_list(filename):
    temperature_list = []
    with open(filename) as file:
        for line in file:
            line_strip = float(line.rstrip(' F\n'))
            
            temperature_list.append(line_strip)
    return temperature_list

def average_temp(temperature_list):
    average = sum(temperature_list) / float(len(temperature_list))
    return round(average, 2)

def main():
    from_unit = 'degreeFahrenheit'
    to_unit = 'degreeCelsius'
    temperature_list = read_file_to_temps_list('temps.txt')
    converted_list = convert_temperature(temperature_list, from_unit, to_unit)
    print(temperature_list)
    print(converted_list)
    print('Average temperature: {} {}'.format(average_temp(converted_list), to_unit))
main()

