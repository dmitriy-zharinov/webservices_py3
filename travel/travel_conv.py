import osa


def convert_length(data_list, from_unit, to_unit):
    result_list = []
    URL1 = 'http://www.webservicex.net/length.asmx?WSDL'
    client1 = osa.client.Client(URL1)
    for data in data_list:
        response = client1.service.ChangeLengthUnit(LengthValue=data, fromLengthUnit=from_unit, toLengthUnit=to_unit)
        result_list.append(round(response, 3))
    return result_list


def strip_number(line):
    line_list = line.split(":")
    line_list[1] = line_list[1].replace(',','')
    line_list[1] = line_list[1].rstrip(' mi\n')
    line_list[1] = line_list[1].lstrip(' ')
    return float(line_list[1])


def read_file_to_travel_length(filename):
   travel_length = []
   with open(filename) as file:
        for line in file:
            line_strip = strip_number(line)
            travel_length.append(line_strip)
   return travel_length

def main():
    from_unit = 'Miles'
    to_unit = 'Kilometers'
    travel_list = read_file_to_travel_length('travel.txt')
    print(travel_list)
    converted_list = convert_length(travel_list, from_unit, to_unit)
    
    print(converted_list)
    print('Overall length: {} {}'.format(sum(travel_list), to_unit))
main()

